# Utiliser les services K8

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Introduction aux services Kubernetes

Nous allons plonger dans l'utilisation des services Kubernetes. Voici les points que nous allons aborder :

1. Types de services
2. Services ClusterIP
3. Services NodePort
4. Services LoadBalancer
5. Démonstration pratique de l'utilisation des services dans notre cluster Kubernetes

# Types de services

Chaque service dans Kubernetes possède un type, et ce type détermine comment et où le service va exposer votre application. Il existe quatre types de services : 
- ClusterIP, 
- NodePort, 
- LoadBalancer 
- ExternalName. 

Le type ExternalName est en dehors du cadre de la certification Kubernetes (CKA), donc nous nous concentrerons principalement sur les trois premiers.

# Services ClusterIP

Les services ClusterIP exposent les applications à l'intérieur du réseau du cluster. Ils sont utilisés lorsque les clients sont d'autres pods au sein du cluster. Si votre service est consommé par des clients également à l'intérieur du cluster, vous utiliserez un service ClusterIP.

Exemple : 

```yaml
apiVersion: v1
kind: Service
metadata:
  name: svc-clusterip
spec:
  type: ClusterIP
  selector:
    app: svc-example
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

# Services NodePort

Les services NodePort exposent les applications en dehors du réseau du cluster. Ils sont utilisés lorsque les utilisateurs de vos applications sont extérieurs au cluster. Avec un service NodePort, le client n'est pas à l'intérieur du cluster mais à l'extérieur.

Exemple :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: svc-nodeport
spec:
  type: NodePort
  selector:
    app: svc-example
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
      nodePort: 30080
```

# Services LoadBalancer

Les services LoadBalancer exposent également les applications en dehors du réseau du cluster, mais utilisent un équilibrage de charge externe pour le faire. Ce type de service ne fonctionne que dans le contexte des plateformes cloud qui incluent des fonctionnalités d'équilibrage de charge. Par exemple, si vous exécutez Kubernetes sur Amazon Web Services (AWS), un service LoadBalancer créera un équilibrage de charge AWS qui dirigera le trafic vers votre service Kubernetes.

# Démonstration pratique

Passons maintenant à une démonstration pratique pour voir comment utiliser un service dans notre cluster Kubernetes.

# Création d'un déploiement

Je suis connecté à mon serveur de plan de contrôle Kubernetes et je vais créer un déploiement nommé `deployment-svc-example`.

```bash
nano deployment-svc-example.yml
```

Ajoutez le contenu suivant au fichier :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: deployment-svc-example
spec:
  replicas: 3
  selector:
    matchLabels:
      app: svc-example
  template:
    metadata:
      labels:
        app: svc-example
    spec:
      containers:
      - name: nginx
        image: nginx:1.19.1
        ports:
        - containerPort: 80
```

Créez le déploiement :

```bash
kubectl create -f deployment-svc-example.yml
kubectl get pods
```

# Création d'un service ClusterIP

Créez un fichier YAML pour le service ClusterIP :

```bash
nano svc-clusterip.yml
```

Ajoutez le contenu suivant au fichier :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: svc-clusterip
spec:
  type: ClusterIP
  selector:
    app: svc-example
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
```

Créez le service :

```bash
kubectl create -f svc-clusterip.yml
kubectl get endpoints svc-clusterip
```

# Test du service

Créez un pod de test pour vérifier la connectivité au service :

```bash
nano pod-service-test.yml
```

Ajoutez le contenu suivant au fichier :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-service-test
spec:
  containers:
  - name: busybox
    image: radial/busyboxplus:curl
    command: ['sh', '-c', 'sleep 3600']
```

Créez le pod :

```bash
kubectl create -f pod-service-test.yml
kubectl exec -it pod-service-test -- curl svc-clusterip:80
```

# Création d'un service NodePort

Créez un fichier YAML pour le service NodePort :

```bash
nano svc-nodeport.yml
```

Ajoutez le contenu suivant au fichier :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: svc-nodeport
spec:
  type: NodePort
  selector:
    app: svc-example
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
    nodePort: 30080
```

Créez le service :

```bash
kubectl create -f svc-nodeport.yml
```

# Test du service NodePort

Accédez au service via un navigateur en utilisant l'adresse IP publique de votre nœud de plan de contrôle ou de vos nœuds de travail suinano du port 30080. Par exemple :

```
http://<PUBLIC_IP_ADDRESS>:30080
```

# Conclusion

Dans cette leçon, nous avons discuté des différents types de services Kubernetes, nous avons exploré plus en détail les services ClusterIP et NodePort, et nous avons effectué une démonstration pratique pour créer et utiliser ces services dans notre cluster. C'est tout pour cette leçon. À la prochaine !


# Reférences

https://kubernetes.io/docs/concepts/services-networking/service/